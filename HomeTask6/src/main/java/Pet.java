import java.util.Objects;

public class Pet {
    private String species;
    private String nickName;
    private int age;
    private int trickLevel;

    enum species{
        DOG,FISH,CAT;
    }

    public  Pet(){

    }
    public Pet(int trickLevel){
        this.trickLevel = trickLevel;
    }
    public Pet(String species, String nickName, int age, int trickLevel) {
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void toWelcomeFavourite(){
        System.out.println("Hey,where were you, my swwet "+this.getSpecies());
    }
    public void toDescribeFavourite(){
        System.out.println("My favourite " +this.getSpecies()+ " is " +this.getNickName());
    }
    public void toFeed(){
        System.out.println("I feed my " +this.getSpecies()+ " with provender " );
    }

    public void theFinalize(){
        System.out.println("Pet was deleted");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet pet = (Pet) o;
        return getTrickLevel() == pet.getTrickLevel();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTrickLevel());
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickName='" + nickName + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                '}';
    }
}
