import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class HomeWork_6 {
    public static void main(String[] args) {
        String[][] schedule = new String[7][1];
        schedule[0][0] = "MONDAY-Working";
        schedule[1][0] ="TUESDAY-Swimming";
        schedule[2][0] ="WEDNESDAY-Running";
        schedule[3][0] ="THURSDAY-Biking";
        schedule[4][0] ="FRIDAY-Reading";
        schedule[5][0] ="SATURDAY-Working";
        schedule[6][0] ="SUNDAY-Resting";


        for (Human.DayOfWeeks daily: Human.DayOfWeeks.values()) {
            System.out.println(daily.name()+ " Hobbie " +daily.showHobbies());
        }
        Human human = new Human("Ilqar","Mammadov",1999,128,schedule);
        System.out.println(human.toString());

        Human mother = new Human(schedule);
        mother.setName("Feride");
        mother.setSurname("Mamedova");
        mother.setYear(1967);
        mother.setIQ(134);


        Human father = new Human(schedule);
        father.setName("Muzaffer");
        father.setSurname("Mamedov");
        father.setYear(1957);
        father.setIQ(154);

        System.out.println("Mother's hashcode =  " +mother.hashCode());
        System.out.println("Father's hashcode =  " +father.hashCode());
        System.out.println("Equality of mother and father " +mother.equals(father));

        Human[] children1 = new Human[2];
        children1[0] = new Human(schedule);
        children1[0].setName("Veli");
        children1[0].setSurname("Mamedov");
        children1[0].setYear(1999);
        children1[0].setIQ(134);
        children1[1] = new Human(schedule);
        children1[1].setName("Eli");
        children1[1].setSurname("Mamedov");
        children1[1].setYear(1995);
        children1[1].setIQ(129);

        Pet pet = new Pet();
        Pet pet1 = new Pet(13);
        pet.setAge(2);
        pet.setNickName("Bobby");
        pet.setSpecies("Dog");
        pet.setTrickLevel(13);

        System.out.println("pet's hashcode =  " +pet.hashCode());
        System.out.println("pet1's hashcode =  " +pet1.hashCode());
        System.out.println("Equality of pet and pet1: " +pet.equals(pet1));

        Family family = new Family(mother,father,children1,pet);
        System.out.println(family.toString());
        System.out.println("A number of people in the family are " +family.countFamily());
        ArrayList<Family> humanList = new ArrayList<Family>();
        humanList.add(family);
        for (int i = 0; i <humanList.size(); i++) {
        family.deleteChild(2);
        humanList.get(i);
        }
       /* humanList.add(family.getFather());
        Human[] huma = family.getChildren();
        humanList.add(huma[0]);
        humanList.add(huma[1]);*/
                
        Human newChild = new Human(schedule);
        newChild.setName("Corc");
        newChild.setSurname("Abdullayev");
        newChild.setYear(2000);
        newChild.setIQ(154);
        family.addChild(newChild); //adding a child to the family.

        System.out.println(family.toString());
        System.out.println("A number of people in the family are " +family.countFamily());
        if (family.deleteChild(2)){  //delete a child by its index.
            System.out.println("Succesfully deleted");
            System.out.println("A number of people in the family are " +family.countFamily()); //if you delete a child count of family will decrease.
        }else{
            System.out.println("There is problem in deleted proesses");
        }

        pet.toWelcomeFavourite();
        pet.toDescribeFavourite();
        pet.toFeed();

        Human.show();
        System.gc();

    }
}

